Hi

This is a demo version of the Wordle game. Code from [Dan's video](https://www.youtube.com/watch?v=K77xThbu66A&t=3212s)  

Download the file and you can play with it.  

Also you can add more words in the js file

Have fun!  

-------------------------------------------------

I add a MIT license so any users can reuse the code.